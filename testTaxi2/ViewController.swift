//
//  ViewController.swift
//  testTaxi2
//
//  Created by Stanisaw Sobczyk on 14/07/2017.
//  Copyright © 2017 Stanisaw Sobczyk. All rights reserved.
//
import GoogleMaps
import UIKit
import CoreLocation
import GooglePlaces

class ViewController: UIViewController {

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var zoomLevel: Float = 15.0
    var startPos: (Double?, Double?, String?, String?)
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let startCamera = GMSCameraPosition.camera(withLatitude: 52.1918578, longitude: 21.0087811, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: startCamera)
        
        view = mapView
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Check Position", style: .done, target: self, action: #selector(CheckPos))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Go to", style: .done, target: self, action: #selector(GoTo))
       
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
       
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "lookForPlace" {
          
            let lookForViewController = segue.destination as! LookingForPlacesViewController
            
            
            
            
            lookForViewController.startLocation = GMSMapPoint(x: startPos.0! , y: startPos.1!)
            lookForViewController.startAdress.0 = startPos.2!
            lookForViewController.startAdress.1 = startPos.3!
            
            
        }
    }


    
            
    
        

    
   


            
            
           //StartPlace.reverseGeocodeLocation(currentLocation!, completionHandler: {nil;,nil in })
            
        
            
            

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

     func GoTo() {
        performSegue(withIdentifier: "lookForPlace", sender: nil)
    }
    
    func CheckPos() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        currentLocation = locationManager.location
        if currentLocation != nil {
        GMSCameraPosition.camera(withTarget: (currentLocation?.coordinate)!, zoom: 12.0)    
        startPos.0 = currentLocation?.coordinate.latitude
        startPos.1 = currentLocation?.coordinate.longitude
        startPos.2 = "unknown"
        startPos.3 = "unknown"
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate((currentLocation?.coordinate)! , completionHandler: {response,error in
                //if ((placemarks?.accessibilityElementCount())!>0 ){
              
                    
               // }
                if(error != nil) {

                    self.startPos.2 = "some error"
                }
                if let adres = response?.firstResult() {
                        
                        self.startPos.2 = "\(String(describing: adres.thoroughfare!))"
                        self.startPos.3 = "\(String(describing: adres.locality!)) \(String(describing: adres.country!))"
            }
   
            })
        let changeCamera = GMSCameraPosition.camera(withTarget: (currentLocation?.coordinate)!, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: changeCamera)
        let marker = GMSMarker()
        marker.position = changeCamera.target
        marker.snippet = "Your Position"
        marker.map = mapView
        navigationItem.rightBarButtonItem?.isEnabled = true
        view = mapView
        }
        else {
            let alert = UIAlertController(title: "Mistake", message: "Sorry something went wrong", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            navigationItem.rightBarButtonItem?.isEnabled = false

            
        }
        
    }
    

}



extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        let alert = UIAlertController(title: "Mistake", message: "Sorry something went wrong", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}

