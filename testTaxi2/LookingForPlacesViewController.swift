//
//  LookingForPlacesViewController.swift
//  testTaxi2
//
//  Created by Stanisaw Sobczyk on 15/07/2017.
//  Copyright © 2017 Stanisaw Sobczyk. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class LookingForPlacesViewController: UIViewController {
    let autocompleteController = GMSAutocompleteViewController()
  
    var startLocation: GMSMapPoint?
    var finishLocation: GMSMapPoint?
    var startAdress: (String?,String?)
    var finalAdress: (String?,String?)
    
    // labels with coordinates
    @IBOutlet weak var coordinateLabel: UILabel!
    @IBOutlet weak var coordinateLabel1: UILabel!

    
    //labels with start adress
    @IBOutlet weak var startadress0: UILabel!
    @IBOutlet weak var startadress1: UILabel!
   
    // labels with final adress
    @IBOutlet weak var finaladress0: UILabel!
    @IBOutlet weak var finaladress1: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Show google search field to select final adress
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // show start adress
        coordinateLabel.text = showCoordinates(startLocation)
        if(startAdress.0 != nil && startAdress.1 != nil) {
            startadress0.text = (startAdress.0)!
            startadress1.text = (startAdress.1)!
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func showCoordinates(_ Point:GMSMapPoint?) -> String {
        if Point != nil {
            
            if Point!.x >= 0.0 && Point!.y >= 0.0 {
                return ":\(String(format: "%.2f", Point!.x)) N, \(String(format: "%.2f", Point!.y)) E "
            }
            else if Point!.x < 0.0 && Point!.y > 0.0 {
                return ":\(String(format: "%.2f", Point!.x)) S, \(String(format: "%.2f", Point!.y)) W "
            }
            else if Point!.x > 0.0 && Point!.y < 0.0 {
                return ":\(String(format: "%.2f", Point!.x)) N, \(String(format: "%.2f", Point!.y)) W "
            }
            else {
                return ":\(String(format: "%.2f", Point!.x)) S, \(String(format: "%.2f", Point!.y)) E "
            }
        }
        else {
            return "no coordinates"
        }
    }
    
    
    @IBAction func orderTaxi(_ sender: Any) {
        
        if startAdress.1 == finalAdress.1 {
            if let startStreet = startAdress.0 {
                if let endStreet = finalAdress.0 {
                    let alert = UIAlertController(title: "Order", message: "You are going from \(String(describing: startStreet)) to \(String(describing: endStreet))", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(action)
                    present(alert, animated: true, completion: nil)
                    return
                }
                return
            }
        }
        else {
            let alert = UIAlertController(title: "Order", message: "Your order is unacceptable", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }


}

extension LookingForPlacesViewController: GMSAutocompleteViewControllerDelegate {
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        finishLocation = GMSMapPoint(x: place.coordinate.latitude , y: place.coordinate.longitude)
        coordinateLabel1.text = showCoordinates(finishLocation!)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate((place.coordinate) , completionHandler: {response,error in
            if(error != nil) {
                
                self.finalAdress.0 = "some error"
                self.finalAdress.1 = "some error"
            }
            if let adres = response?.firstResult() {
                self.finalAdress.0 = "\(String(describing: adres.thoroughfare!))"
                self.finalAdress.1 = "\(String(describing: adres.locality!)) \(String(describing: adres.country!))"
                self.finaladress0.text = self.finalAdress.0
                self.finaladress1.text = self.finalAdress.1
                
                
            
            }
           
            
            
            
            
        })
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
   
    dismiss(animated: true, completion: nil)
    navigationController?.popViewController(animated: true)
    }
    
    
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}


